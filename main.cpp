#include "hostinfo_fetcher.h"

int main()
{
  // spawn core subprocess
  ShellExecute(NULL, "open", "bin\\hostinfo-fetcher-go.exe", NULL, NULL, SW_HIDE);
  
  HostInfo hostInfo;
  // start read hostinfo thread
  std::thread readHostInfoThread (readHostInfo, &hostInfo);
  readHostInfoThread.detach();
  
  // create the main window
  sf::RenderWindow window(sf::VideoMode(360, 220), "hostinfo-fetcher", sf::Style::None);
  // set vertical sync
  window.setVerticalSyncEnabled(true);
  // set topmost
  SetWindowPos(window.getSystemHandle(), HWND_TOPMOST, window.getPosition().x, window.getPosition().y, window.getSize().x, window.getSize().y, 0);
  //// set tranparent
  // Set WS_EX_LAYERED on this window 
  SetWindowLong(window.getSystemHandle(), GWL_EXSTYLE, GetWindowLong(window.getSystemHandle(), GWL_EXSTYLE) | WS_EX_LAYERED);
  // 80% alpha
  SetLayeredWindowAttributes(window.getSystemHandle(), 0, (255 * 80) / 100, LWA_ALPHA);
  
  // load font
  sf::Font appFont;
  if (!appFont.loadFromFile("assets/msyh.ttc"))
    return EXIT_FAILURE;

  sf::Color textColor(0, 0, 0);
  sf::Color bgColor(255, 168, 168);
  int fontSize = 16;
  
  // initialize texts
  // exclude cpu usage
  // sf::Text cpuPercentText("", appFont, fontSize);
  // cpuPercentText.setFillColor(sf::Color(0, 0, 0, 255));
  // cpuPercentText.setPosition(4, 4);
  sf::Text currentUserText("", appFont, fontSize);
  currentUserText.setFillColor(textColor);
  currentUserText.setPosition(4, 4);
  sf::Text totalMemText("", appFont, fontSize);
  totalMemText.setFillColor(textColor);
  totalMemText.setPosition(4, 22);
  sf::Text usedMemText("", appFont, fontSize);
  usedMemText.setFillColor(textColor);
  usedMemText.setPosition(4, 40);
  sf::Text totalDiskText("", appFont, fontSize);
  totalDiskText.setFillColor(textColor);
  totalDiskText.setPosition(4, 58);
  sf::Text usedDiskText("", appFont, fontSize);
  usedDiskText.setFillColor(textColor);
  usedDiskText.setPosition(4, 76);

  bool mousePressedFlag = false;
  sf::Vector2i cursorPosRel;
  sf::Event event;
  // std::wstring cpuPercentWString;
  std::wstring currentUserWString;
  std::wstring totalMemWString;
  std::wstring usedMemWString;
  std::wstring totalDiskWString;
  std::wstring usedDiskWString;
  std::list<sf::Text> interfaceNameSfmlTextList;
  std::list<sf::Text> interfaceIPSfmlTextList;
  std::list<sf::Text> printerNameSfmlTextList;
  
  while (window.isOpen()) {
    
    // process events
    while (window.pollEvent(event)) {
      
      // close window: exit
      if (event.type == sf::Event::Closed)
	window.close();

      // mouse pressed event
      if (event.type == sf::Event::MouseButtonPressed) {
	mousePressedFlag = true;
	cursorPosRel = sf::Mouse::getPosition(window);
      }
      else if (event.type == sf::Event::MouseButtonReleased) {
	mousePressedFlag = false;
      }
      if (event.type == sf::Event::MouseMoved && mousePressedFlag == true) {
	sf::Vector2i cursorPosition = sf::Mouse::getPosition();
	sf::Vector2i desiredWindowPosition;
	desiredWindowPosition.x = cursorPosition.x - cursorPosRel.x;
	desiredWindowPosition.y = cursorPosition.y - cursorPosRel.y;
	window.setPosition(desiredWindowPosition);
      }

      // catch the resize events, let everything stay the same size
      if (event.type == sf::Event::Resized)
	{
	  // update the view to the new size of the window
	  sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
	  window.setView(sf::View(visibleArea));
	}
    }
    
    // clear screen
    window.clear(bgColor);
    
    //// draw hostinfo text
    // convert HostInfo std::string member to std::wstring for chinese string concatenation
    // cpuPercentWString.assign(hostInfo.cpuPercent.begin(), hostInfo.cpuPercent.end());
    currentUserWString.assign(hostInfo.currentUser.begin(), hostInfo.currentUser.end());
    totalMemWString.assign(hostInfo.totalMemory.begin(), hostInfo.totalMemory.end());
    usedMemWString.assign(hostInfo.usedMemoryPercent.begin(), hostInfo.usedMemoryPercent.end());
    totalDiskWString.assign(hostInfo.totalDiskVolumn.begin(), hostInfo.totalDiskVolumn.end());
    usedDiskWString.assign(hostInfo.usedDiskVolumn.begin(), hostInfo.usedDiskVolumn.end());
    
    // cpuPercentText.setString(L"CPU 使用率: " + cpuPercentWString);
    currentUserText.setString(L"当前用户: " + currentUserWString);
    totalMemText.setString(L"总内存: " + totalMemWString);
    usedMemText.setString(L"已使用: " + usedMemWString);
    totalDiskText.setString(L"C盘总容量: " + totalDiskWString);
    usedDiskText.setString(L"已使用: " + usedDiskWString);

    //// interface stats
    // clear list firstly
    interfaceNameSfmlTextList.clear();
    interfaceIPSfmlTextList.clear();
    for (std::list<HostInfoInterfaceStat>::iterator iter = hostInfo.interfaceStatList.begin(); iter != hostInfo.interfaceStatList.end(); ++iter) {
      std::wstring interfaceNameWString;
      std::wstring interfaceIPWString;
      interfaceNameWString.assign(iter->interfaceName.begin(), iter->interfaceName.end());
      interfaceIPWString.assign(iter->interfaceAddr.begin(), iter->interfaceAddr.end());
      interfaceNameSfmlTextList.push_back(sf::Text(L"网络接口: " + interfaceNameWString, appFont, fontSize));
      interfaceIPSfmlTextList.push_back(sf::Text(L"IP: " + interfaceIPWString, appFont, fontSize));
    }

    // printer names
    printerNameSfmlTextList.clear();
    for (std::string printerNameString : hostInfo.sharedPrinterNameList) {
      std::wstring printerNameWString;
      printerNameWString.assign(printerNameString.begin(), printerNameString.end());
      printerNameSfmlTextList.push_back(sf::Text(L"打印机: " + printerNameWString, appFont, fontSize));
    }

    // add all sf::Text to a list for get the widest text, then set window size correspondingly
    std::list<sf::Text> allAppTextList;
    allAppTextList.push_back(currentUserText);
    allAppTextList.push_back(totalMemText);
    allAppTextList.push_back(usedMemText);
    allAppTextList.push_back(totalDiskText);
    allAppTextList.push_back(usedDiskText);
    for (sf::Text sfmlText : interfaceNameSfmlTextList) {
      allAppTextList.push_back(sfmlText);
    }
    for (sf::Text sfmlText : interfaceIPSfmlTextList) {
      allAppTextList.push_back(sfmlText);
    }
    // set window size
    int maxTextWidth = 0;
    for (sf::Text sfmlText : allAppTextList) {
      if (sfmlText.getLocalBounds().width > maxTextWidth) {
	maxTextWidth = sfmlText.getLocalBounds().width;
      }
    }
    window.setSize(sf::Vector2u(maxTextWidth+8, static_cast<int>(allAppTextList.size())*18+8));
    
    // window.draw(cpuPercentText);
    window.draw(currentUserText);
    window.draw(totalMemText);
    window.draw(usedMemText);
    window.draw(totalDiskText);
    window.draw(usedDiskText);

    int tmpInt_0 = 94;
    for (sf::Text sfmlText : interfaceNameSfmlTextList) {
      sfmlText.setFillColor(textColor);
      sfmlText.setPosition(4, tmpInt_0);
      window.draw(sfmlText);
      tmpInt_0 += 36;
    }
    int tmpInt_1 = 112;
    for (sf::Text sfmlText : interfaceIPSfmlTextList) {
      sfmlText.setFillColor(textColor);
      sfmlText.setPosition(4, tmpInt_1);
      window.draw(sfmlText);
      tmpInt_1 += 36;
    }

    int tmpInt_2 = tmpInt_1 - 18;
    for (sf::Text sfmlText : printerNameSfmlTextList) {
      sfmlText.setFillColor(textColor);
      sfmlText.setPosition(4, tmpInt_2);
      window.draw(sfmlText);
      tmpInt_2 += 18;
    }

    // update the window
    window.display();
  }

  // terminate core binary
  std::system("taskkill /F /IM hostinfo-fetcher-go.exe");
  // destroy thread
  readHostInfoThread.~thread();
  return EXIT_SUCCESS;
}
