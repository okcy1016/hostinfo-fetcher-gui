#include "hostinfo_fetcher.h"

HostInfo readHostInfoFromJson(std::string filePath)
{
  Json::Reader jsonReader;
  Json::Value jsonRoot;
  HostInfo hostInfo;
  
  std::ifstream ifstream_0;
  ifstream_0.open(filePath, std::ios::binary);

  if(jsonReader.parse(ifstream_0,jsonRoot)) {
    hostInfo.currentUser = jsonRoot["CurrentUser"].asString();
    hostInfo.cpuPercent = jsonRoot["CpuPrecent"].asString();
    hostInfo.totalMemory = jsonRoot["TotalMemory"].asString();
    hostInfo.usedMemoryPercent = jsonRoot["UsedMemoryPercent"].asString();
    hostInfo.totalDiskVolumn = jsonRoot["TotalDiskVolumn"].asString();
    hostInfo.usedDiskVolumn = jsonRoot["UsedDiskVolumn"].asString();
    // read array interface stat and printer name
    for (int i = 0; i < jsonRoot["InterfacesStat"].size(); i++) {
      hostInfo.interfaceStatList.push_back(HostInfoInterfaceStat {jsonRoot["InterfacesStat"][i]["InterfaceName"].asString(), jsonRoot["InterfacesStat"][i]["InterfaceAddr"].asString()});
    }
    for (int i = 0; i < jsonRoot["SharedPrintersName"].size(); i++) {
      hostInfo.sharedPrinterNameList.push_back(jsonRoot["SharedPrintersName"][i].asString());
    }
  }
  ifstream_0.close();
  
  return hostInfo;
}

void readHostInfo(HostInfo* hostInfoPtr) {
  while (true) {
    // waiting for core binary generate hostinfo.json
    Sleep(1000);
    HostInfo hostInfoTmp = readHostInfoFromJson("hostinfo.json");
    *hostInfoPtr = hostInfoTmp;
    Sleep(30000);
  }
}
