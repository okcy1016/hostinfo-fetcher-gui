#include <iostream>
#include <list>
#include <SFML/Graphics.hpp>
#include <windows.h>
#include <thread>
#include <fstream>
#include <json/json.h>
#include <cstdlib>

struct HostInfoInterfaceStat {
  std::string interfaceName;
  std::string interfaceAddr;
};
  
struct HostInfo {
  std::string currentUser;
  std::string cpuPercent;
  std::string totalMemory;
  std::string usedMemoryPercent;
  std::string totalDiskVolumn;
  std::string usedDiskVolumn;
  std::list<HostInfoInterfaceStat> interfaceStatList;
  std::list<std::string> sharedPrinterNameList;
};

HostInfo readHostInfoFromJson(std::string);
void readHostInfo(HostInfo*);
