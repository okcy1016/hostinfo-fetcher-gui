app: main.o read_hostinfo.o
	g++ main.o read_hostinfo.o -o hostinfo-fetcher.exe -lsfml-graphics -lsfml-window -lsfml-system -ljsoncpp

all:
	g++ -c read_hostinfo.cpp
	g++ -c main.cpp
	g++ main.o read_hostinfo.o -o hostinfo-fetcher.exe -lsfml-graphics -lsfml-window -lsfml-system -ljsoncpp

main:
	g++ -c main.cpp
	g++ main.o read_hostinfo.o -o hostinfo-fetcher.exe -lsfml-graphics -lsfml-window -lsfml-system -ljsoncpp

read_hostinfo:
	g++ -w -c read_hostinfo.cpp
	g++ main.o read_hostinfo.o -o hostinfo-fetcher.exe -lsfml-graphics -lsfml-window -lsfml-system -ljsoncpp

release:
	g++ -c main.cpp
	g++ -c read_hostinfo.cpp
	g++ main.o read_hostinfo.o -o hostinfo-fetcher.exe -lsfml-graphics -lsfml-window -lsfml-system -ljsoncpp -s -mwindows

clean:
	rm *.o
